package com.example.magiag.cursoandroid.repository;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.magiag.cursoandroid.util.Constantes;
import com.example.magiag.cursoandroid.util.Util;

/**
 * Created by Magiag on 21/08/2016.
 */
public class LoginRepository extends SQLiteOpenHelper {

    public LoginRepository(Context context) {
        super(context, Constantes.BD_NOME, null, Constantes.BD_VERSAO);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        StringBuilder query = new StringBuilder();
        query.append("CREATE IF NOT EXISTS TABLE TB_LOGIN( ");
        query.append(" ID_LOGIN INTEGER PRIMARY KEY AUTOINCREMENT,");
        query.append(" USUARIO TEXT(15) NOT NULL,");
        query.append(" SENHA TEXT(15) NOT NULL)");

        db.execSQL(query.toString());

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    public void popularBD() {
        StringBuilder query = new StringBuilder();
        query.append("INSERT INTO TB_LOGIN(USUARIO, SENHA) VALUES(?, ?)");

        SQLiteDatabase db = getWritableDatabase();
        db.execSQL(query.toString(), new String[]{"admin", "admin"});
    }

    public void listarLogins(Activity activity) {
        SQLiteDatabase db = getReadableDatabase();
        StringBuilder sb = new StringBuilder();
        Cursor cursor = db.query("TB_LOGIN", null, "ID_LOGIN = ? and USUARIO = ?", new String[]{"1", "admin"}, null, null, "USUARIO");
        while (cursor.moveToNext()) {
            sb.append("ID de Usuário: ");
            sb.append(String.valueOf(cursor.getColumnIndex("ID_LOGIN")));
            sb.append("\nNome de Usuário: ");
            sb.append(cursor.getString(cursor.getColumnIndex("USUARIO")));
            sb.append("\nSenha de Usuário: ");
            sb.append(cursor.getString(cursor.getColumnIndex("SENHA")));

            Util.showMsgToast(activity, sb.toString());
        }
    }

    public void addLogin(String login, String senha) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("USUARIO", login);
        contentValues.put("SENHA", senha);

        db.insert("TB_LOGIN", null, contentValues);
    }

    public void updateLogin(String login, String senha) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("USUARIO", login);
        contentValues.put("SENHA", senha);

        db.update("TB_LOGIN", contentValues,"ID_LOGIN > 1",null);
    }

    public void deleteLogin(String login, String senha){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete("TB_LOGIN","USUARIO = ? or SENHA = ?",new String[]{login, senha});
    }
}
