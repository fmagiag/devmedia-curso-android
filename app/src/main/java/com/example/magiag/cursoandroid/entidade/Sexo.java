package com.example.magiag.cursoandroid.entidade;

/**
 * Created by Magiag on 27/08/2016.
 */
public enum Sexo {
    MASCULINO, FEMININO
}
