package com.example.magiag.cursoandroid.bo;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import com.example.magiag.cursoandroid.repository.LoginRepository;
import com.example.magiag.cursoandroid.util.Util;
import com.example.magiag.cursoandroid.validation.LoginValidation;

/**
 * Created by Magiag on 21/08/2016.
 */
public class LoginBO {

    private LoginRepository loginRepository;

    public LoginBO(Activity activity){
        loginRepository = new LoginRepository(activity);
        loginRepository.popularBD();
        loginRepository.listarLogins(activity);
    }

    public boolean validarCamposLogin(LoginValidation validation) {
        boolean resultado = true;
        if (validation.getLogin() == null || "".equals(validation.getLogin())) {
            validation.getEdtLogin().setError("Campo Obrigatório!");
//            Util.showMsgToast(LoginActivity.this,"Campo Login obrigatório");
            resultado = false;
        } else if (validation.getLogin().length() <= 3) {
            validation.getEdtLogin().setError("Campo deve ter no mínimo 3 caracteres!");
        }

        if (validation.getSenha() == null || "".equals(validation.getSenha())) {
            validation.getEdtSenha().setError("Campo Obrigatório!");
//            Util.showMsgToast(LoginActivity.this,"Campo Senha obrigatório");
            resultado = false;
        }

        if (resultado) {
            loginRepository.addLogin(validation.getLogin(),validation.getSenha());
            if (!validation.getLogin().equals("admin") || !validation.getSenha().equals("admin")) {
                Util.showMsgToast(validation.getActivity(), "Lofin/Senha inválidos!");
                resultado = false;
            } else {
                SharedPreferences.Editor editor = validation.getActivity().getSharedPreferences("pref",Context.MODE_PRIVATE).edit();
                editor.putString("login", validation.getLogin());
                editor.putString("senha", validation.getSenha());
                editor.commit();
            }
        }
        return resultado;
    }

    public void deslogar(){

    }
}
