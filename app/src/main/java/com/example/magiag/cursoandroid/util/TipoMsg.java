package com.example.magiag.cursoandroid.util;

/**
 * Created by Magiag on 19/08/2016.
 */
public enum TipoMsg {

    ERRO, INFO, SUCESSO, ALERTA
}
